package org.shashaka.io.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostPrivateInfo implements Serializable {
    private static final long serialVersionUID = -7855687705467273960L;

    private String privateName;
}
