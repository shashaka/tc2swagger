package org.shashaka.io.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostInfo implements Serializable {
    private static final long serialVersionUID = 4874576524392062751L;

    private String info;

    private PostPrivateInfo postPrivateInfo;
}
