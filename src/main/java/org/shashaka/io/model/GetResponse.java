package org.shashaka.io.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetResponse implements Serializable {
    private static final long serialVersionUID = -1337816851604294267L;

    private String name;
}
