package org.shashaka.io.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostRequest implements Serializable {

    private static final long serialVersionUID = -7450884245271689208L;

    private String name;

    private PostInfo postInfo;
}
