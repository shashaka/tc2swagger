package org.shashaka.io.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostResponse implements Serializable {

    private static final long serialVersionUID = -129382044653880130L;
    
    private String name;
}
