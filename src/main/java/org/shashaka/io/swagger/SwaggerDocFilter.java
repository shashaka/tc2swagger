package org.shashaka.io.swagger;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.shashaka.io.swagger.utils.SwaggerGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

@Slf4j
@TestComponent
public class SwaggerDocFilter implements Filter {

    @Autowired
    private SwaggerGenerator swaggerGenerator;

    @SneakyThrows
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) {

        MockHttpServletRequest mockHttpServletRequest = (MockHttpServletRequest) request;
        chain.doFilter(request, response);

        MockHttpServletResponse mockHttpServletResponse = (MockHttpServletResponse) response;

        swaggerGenerator.addModel(mockHttpServletRequest, mockHttpServletResponse);
    }


}
