package org.shashaka.io.swagger;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;

@Slf4j
@TestConfiguration
@ComponentScan
public class SwaggerDocConfig {

    @Autowired
    private SwaggerDocFilter swaggerDocFilter;

    @Bean
    public MockMvc mockMvc(DefaultMockMvcBuilder builder) {
        return builder
                .addFilters(swaggerDocFilter)
                .build();
    }
}
