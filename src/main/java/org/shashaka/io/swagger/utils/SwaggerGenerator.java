package org.shashaka.io.swagger.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.extern.slf4j.Slf4j;
import org.shashaka.io.swagger.model.doc.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestComponent;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Parameter;
import java.nio.charset.StandardCharsets;
import java.util.*;

@TestComponent
@Slf4j
public class SwaggerGenerator {

    private static final String SWAGGER_IN_PATH = "path";
    private static final String SWAGGER_IN_QUERY = "query";
    private static final String SWAGGER_IN_HEADER = "header";
    private static final String SWAGGER_IN_BODY = "body";

    @Autowired
    private RequestMappingHandlerMapping handlerMapping;

    @Autowired
    private ObjectMapper objectMapper;

    private static SwaggerDocument document;
    private static Map<String, SwaggerDefinition> definitions;
    private static SwaggerInfo info;

    @PostConstruct
    public void init() {
        document = new SwaggerDocument();

        info = new SwaggerInfo();
        info.setVersion("1.0.0");
        info.setDescription("test API Doc");
        info.setTitle("Swagger API Doc from TC");
        document.setInfo(info);

        definitions = new HashMap<>();
        document.setDefinitions(definitions);

    }

    public void writeYaml() throws IOException {
        ObjectMapper om = new ObjectMapper(new YAMLFactory());
        om.writeValue(new File("swagger.yaml"), document);
    }

    private String addPath(HandlerMethod handler) {
        String path = getPath(handler);
        if (document.getPaths() == null) {
            document.setPaths(new HashMap<>());
        }
        document.getPaths().computeIfAbsent(path, k -> new HashMap<>());
        return path;
    }

    public void addModel(MockHttpServletRequest mockHttpServletRequest, MockHttpServletResponse mockHttpServletResponse) throws Exception {

        HandlerMethod handler = (HandlerMethod) handlerMapping.getHandler(mockHttpServletRequest).getHandler();

        addRequestBodyModel(mockHttpServletRequest, handler);
        addResponseBodyModel(mockHttpServletResponse, handler);
        String path = addPath(handler);
        String httpMethod = addHttpMethod(path, mockHttpServletRequest.getMethod().toLowerCase());

        SwaggerPath swaggerPath = document.getPaths().get(path).get(httpMethod);
        swaggerPath.setTags(Collections.singletonList(handler.getBeanType().getSimpleName()));
        swaggerPath.setParameters(getRequestParameters(mockHttpServletRequest, handler));
        swaggerPath.setResponses(getResponses(handler));
    }

    private Map<String, SwaggerResponse> getResponses(HandlerMethod handler) {
        Class<?> responseBodyClass = getResponseBodyClass(handler);
        Map<String, SwaggerResponse> swaggerResponseMap = new HashMap<>();
        SwaggerResponse successResponseValue = new SwaggerResponse();

        SwaggerSchema successResponseSchema = new SwaggerSchema();
        successResponseSchema.setRef("#/definitions/" + responseBodyClass.getSimpleName());
        successResponseValue.setSchema(successResponseSchema);
        successResponseValue.setDescription("success");

        swaggerResponseMap.put("200", successResponseValue);
        return swaggerResponseMap;
    }

    private List<SwaggerPrameter> getRequestParameters(MockHttpServletRequest request, HandlerMethod handler) {
        List<SwaggerPrameter> swaggerPrameters = new ArrayList<>();

        Set<Parameter> pathVariables = getPathVariable(handler);
        for (Parameter pathVariable : pathVariables) {
            SwaggerPrameter pathVariableParameter = new SwaggerPrameter();
            pathVariableParameter.setIn(SWAGGER_IN_PATH);
            pathVariableParameter.setName(pathVariable.getName());
            pathVariableParameter.setRequired(true);
            pathVariableParameter.setType(pathVariable.getType().getSimpleName().toLowerCase());
            swaggerPrameters.add(pathVariableParameter);
        }

        Set<Parameter> requestQueries = getRequestQuery(handler);
        for (Parameter requestQuery : requestQueries) {
            SwaggerPrameter requestQueryParameter = new SwaggerPrameter();
            requestQueryParameter.setIn(SWAGGER_IN_QUERY);
            requestQueryParameter.setName(requestQuery.getName());
            requestQueryParameter.setType(requestQuery.getType().getSimpleName().toLowerCase());
            String queryVal = request.getParameter(requestQuery.getName());
            if (queryVal != null && !queryVal.isEmpty()) {
                requestQueryParameter.setDefaultValue(queryVal);
            }
            swaggerPrameters.add(requestQueryParameter);
        }

        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            SwaggerPrameter requestHeaderParameter = new SwaggerPrameter();
            requestHeaderParameter.setIn(SWAGGER_IN_HEADER);
            requestHeaderParameter.setName(headerName);
            requestHeaderParameter.setType(request.getHeader(headerName).getClass().getSimpleName().toLowerCase());

            String headerVal = request.getHeader(requestHeaderParameter.getName());
            if (headerVal != null && !headerVal.isEmpty()) {
                requestHeaderParameter.setDefaultValue(headerVal);
            }

            swaggerPrameters.add(requestHeaderParameter);
        }

        Parameter requestBodyClass = getRequestBodyClass(handler);
        if (requestBodyClass != null) {
            SwaggerPrameter requestBodyParameter = new SwaggerPrameter();
            requestBodyParameter.setIn(SWAGGER_IN_BODY);
            requestBodyParameter.setName(SWAGGER_IN_BODY);
            requestBodyParameter.setRequired(true);
            SwaggerSchema requestBodySchema = new SwaggerSchema();
            requestBodySchema.setRef("#/definitions/" + requestBodyClass.getType().getSimpleName());
            requestBodyParameter.setSchema(requestBodySchema);
            swaggerPrameters.add(requestBodyParameter);
        }

        return swaggerPrameters;
    }

    private String addHttpMethod(String path, String method) {
        document.getPaths().get(path).computeIfAbsent(method, k -> new SwaggerPath());
        return method;
    }

    private void addResponseBodyModel(MockHttpServletResponse response, HandlerMethod handler) throws JsonProcessingException {
        Class<?> responseBodyType = getResponseBodyClass(handler);

        String body = new String(response.getContentAsByteArray(), StandardCharsets.UTF_8);
        Object bodyClass = objectMapper.readValue(body, responseBodyType);

        addBodySwaggerModel(responseBodyType, bodyClass);
    }

    private void addBodySwaggerModel(Class<?> bodyType, Object bodyClass) {
        SwaggerDefinition swaggerDefinition = new SwaggerDefinition();
        Map<String, PropertyMap> properties = new HashMap<>();

        for (Field declaredField : bodyType.getDeclaredFields()) {
            if (!java.lang.reflect.Modifier.isStatic(declaredField.getModifiers())) {
                PropertyMap propertyMap = new PropertyMap();
                declaredField.setAccessible(true);
                Object fieldValue = null;
                if (bodyClass != null) {
                    fieldValue = ReflectionUtils.getField(declaredField, bodyClass);
                }
                if (declaredField.getType().getName().contains("java")) {
                    propertyMap.setType(declaredField.getType().getSimpleName().toLowerCase());
                    if (fieldValue != null) {
                        propertyMap.setExample(fieldValue.toString());
                    }
                } else {
                    propertyMap.setRef("#/definitions/" + declaredField.getType().getSimpleName());
                    addBodySwaggerModel(declaredField.getType(), fieldValue);
                }
                properties.put(declaredField.getName(), propertyMap);
                swaggerDefinition.setProperties(properties);
            }
        }
        definitions.put(bodyType.getSimpleName(), swaggerDefinition);
    }

    private void addRequestBodyModel(MockHttpServletRequest request, HandlerMethod handler) throws JsonProcessingException {
        Parameter requestBodyClass = getRequestBodyClass(handler);
        if (requestBodyClass == null) {
            return;
        }
        Class<?> requestBodyType = requestBodyClass.getType();

        String body = new String(request.getContentAsByteArray(), StandardCharsets.UTF_8);
        Object bodyClass = objectMapper.readValue(body, requestBodyType);

        addBodySwaggerModel(requestBodyType, bodyClass);
    }

    private String getPath(HandlerMethod handler) {
        String prefix = "";
        String path = "";

        RequestMapping classRequestMapping = handler.getBeanType().getAnnotation(RequestMapping.class);
        if (classRequestMapping != null) {
            prefix = classRequestMapping.value()[0];
        }

        String[] pathValues = getRequestMappingAnnotation(handler);

        if (!Arrays.asList(pathValues).isEmpty()) {
            path = pathValues[0];
        }

        return prefix + path;
    }

    private String[] getRequestMappingAnnotation(HandlerMethod handler) {
        String[] value = {};
        for (MethodParameter methodParameter : handler.getMethodParameters()) {
            if (methodParameter.getMethodAnnotation(RequestMapping.class) != null) {
                return methodParameter.getMethodAnnotation(RequestMapping.class).value();
            } else if (methodParameter.getMethodAnnotation(PostMapping.class) != null) {
                return methodParameter.getMethodAnnotation(PostMapping.class).value();
            } else if (methodParameter.getMethodAnnotation(GetMapping.class) != null) {
                return methodParameter.getMethodAnnotation(GetMapping.class).value();
            } else if (methodParameter.getMethodAnnotation(PutMapping.class) != null) {
                return methodParameter.getMethodAnnotation(PutMapping.class).value();
            } else if (methodParameter.getMethodAnnotation(DeleteMapping.class) != null) {
                return methodParameter.getMethodAnnotation(DeleteMapping.class).value();
            }
        }
        return value;
    }

    private Set<Parameter> getPathVariable(HandlerMethod handler) {
        Set<Parameter> pathVariables = new HashSet<>();
        for (MethodParameter methodParameter : handler.getMethodParameters()) {
            if (methodParameter.getParameterAnnotation(PathVariable.class) != null) {
                pathVariables.add(methodParameter.getParameter());
            }
        }
        return pathVariables;
    }

    private Set<Parameter> getRequestQuery(HandlerMethod handler) {
        Set<Parameter> requestQuery = new HashSet<>();
        for (MethodParameter methodParameter : handler.getMethodParameters()) {
            if (methodParameter.getParameterAnnotation(RequestParam.class) != null) {
                requestQuery.add(methodParameter.getParameter());
            }
        }
        return requestQuery;
    }

    private Parameter getRequestBodyClass(HandlerMethod handler) {
        for (MethodParameter methodParameter : handler.getMethodParameters()) {
            if (methodParameter.getParameterAnnotation(RequestBody.class) != null) {
                return methodParameter.getParameter();
            }
        }
        return null;
    }

    private Class<?> getResponseBodyClass(HandlerMethod handler) {
        return handler.getMethod().getReturnType();
    }
}
