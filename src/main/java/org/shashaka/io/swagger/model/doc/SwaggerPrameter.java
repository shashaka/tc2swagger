package org.shashaka.io.swagger.model.doc;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwaggerPrameter {
    private String name;
    private String in;
    private String description;
    private String type;
    private Boolean required;
    private SwaggerSchema schema;

    @JsonProperty("default")
    private String defaultValue;
}
