package org.shashaka.io.swagger.model.doc;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwaggerDefinition {
    private String type = "object";
    private List<String> required;
    private Map<String, PropertyMap> properties;
}
