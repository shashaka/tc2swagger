package org.shashaka.io.swagger.model.doc;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwaggerDocument {

    private String swagger = "2.0";
    private SwaggerInfo info;
    private Map<String, Map<String, SwaggerPath>> paths;
    private Map<String, SwaggerDefinition> definitions;
}
