package org.shashaka.io.swagger.model.doc;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SwaggerPath {
    private List<String> tags;
    private String summary;
    private String description;
    private List<String> consumes;
    private List<String> produces;
    private List<SwaggerPrameter> parameters;
    private Map<String, SwaggerResponse> responses;
}
