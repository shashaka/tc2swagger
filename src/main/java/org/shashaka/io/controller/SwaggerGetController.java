package org.shashaka.io.controller;

import org.shashaka.io.model.GetResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/swagger")
public class SwaggerGetController {

    @GetMapping(value = "/post/{id}")
    public GetResponse getMethod(@PathVariable(required = false) Integer id,
                                 @RequestParam(required = false) String latest,
                                 @RequestParam(required = false) String first) {
        return new GetResponse(latest + first);
    }
}
