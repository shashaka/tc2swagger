package org.shashaka.io.controller;

import org.shashaka.io.model.PostRequest;
import org.shashaka.io.model.PostResponse;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/swagger")
public class SwaggerPostController {

    @PostMapping(value = "/post/{id}")
    public PostResponse postMethod(@PathVariable(required = false) Integer id,
                                   @RequestParam(required = false) String latest,
                                   @RequestParam(required = false) String first,
                                   @RequestBody PostRequest request) {
        return new PostResponse(request.getName() + latest + first);
    }

}
