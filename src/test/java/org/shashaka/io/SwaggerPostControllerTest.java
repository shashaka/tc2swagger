package org.shashaka.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.shashaka.io.model.PostInfo;
import org.shashaka.io.model.PostPrivateInfo;
import org.shashaka.io.model.PostRequest;
import org.shashaka.io.swagger.SwaggerDocConfig;
import org.shashaka.io.swagger.utils.SwaggerGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Slf4j
@Import(SwaggerDocConfig.class)
@WebMvcTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SwaggerPostControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SwaggerGenerator swaggerGenerator;

    @SneakyThrows
    @AfterAll
    public void afterAll() {
        swaggerGenerator.writeYaml();
    }

    @Test
    public void post_test() throws Exception {

        PostRequest request = new PostRequest();
        request.setName("name");
        PostInfo postInfo = new PostInfo();
        PostPrivateInfo postPrivateInfo = new PostPrivateInfo();
        postPrivateInfo.setPrivateName("privateName");
        postInfo.setPostPrivateInfo(postPrivateInfo);
        request.setPostInfo(postInfo);
        String body = objectMapper.writeValueAsString(request);

        mockMvc.perform(
                post("/swagger/post/1?first=myFirst&latest=myLatest")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(body.getBytes()))
                .andDo(print())
                .andExpect(content().json("{\"name\":\"namemyLatestmyFirst\"}"))
                .andExpect(status().isOk());
    }

    @Test
    public void get_test() throws Exception {

        PostRequest request = new PostRequest();
        request.setName("name");
        PostInfo postInfo = new PostInfo();
        PostPrivateInfo postPrivateInfo = new PostPrivateInfo();
        postPrivateInfo.setPrivateName("privateName");
        postInfo.setPostPrivateInfo(postPrivateInfo);
        request.setPostInfo(postInfo);
        String body = objectMapper.writeValueAsString(request);

        mockMvc.perform(
                get("/swagger/post/1?first=myFirst&latest=myLatest"))
                .andDo(print())
                .andExpect(content().json("{\"name\":\"myLatestmyFirst\"}"))
                .andExpect(status().isOk());
    }
}
